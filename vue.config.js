const path = require('path')

module.exports = {
  lintOnSave: true,
  configureWebpack: {
    entry: path.join(__dirname, 'components/FormSchemaVueMaterial.vue'),
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'FormSchemaVueMaterial.js',
      libraryTarget: 'umd',
      library: 'FormSchemaVueMaterial',
      umdNamedDefine: true
    },
    externals: {
      '@formschema/native': '@formschema/native',
      'vue-material': 'vue-material'
    }
  }
}
