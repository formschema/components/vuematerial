# FormSchema VueMaterial
A VueMaterial component wrapper of FormSchema.

[![Build Status](https://travis-ci.org/formschema/vuematerial.svg?branch=master)](https://travis-ci.org/formschema/vuematerial) [![Coverage Status](https://coveralls.io/repos/github/formschema/vuematerial/badge.svg?branch=master)](https://coveralls.io/github/formschema/vuematerial?branch=master)

## Install
```sh
npm install --save @formschema/vuematerial
```

## License
Under the MIT license. See [LICENSE](https://github.com/formschema/vuematerial/blob/master/LICENSE) file for more details.
